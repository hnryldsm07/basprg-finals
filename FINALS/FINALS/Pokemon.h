#pragma once
#include <string>

using namespace std;

class Pokemons
{
	public:
		Pokemons();
		Pokemons(string name, int baseHp, int hp, int maxHp, int level, int baseDamage);
		string name;
		Pokemons* pokemon;
		int baseHp;
		int hp;
		int maxHp;
		int level;
		int baseDamage;
		int exp;
		int expToNextLevel;

		void pokemonList();
		void displayStats(Pokemons* pokemon);
		void pokemonCenter();
		void pokemonRandom();
};